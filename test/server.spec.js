const request = require('supertest')
const app = require('../server')

describe('Server', () => {
    it('Server was set up successfully', (done) => {
        request(app)
        .get('/')
        .expect({ message: 'Welcome to Hollajobs' }, done)
    }) 
})