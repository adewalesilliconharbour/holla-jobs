const express = require('express');

const app = express();

const PORT = 3000
const hostname = '0.0.0.0'

app.get('/', (req, res, next) => {
    return res.json({message: "Welcome to Hollajobs"})
})

app.listen(PORT, () => {
    console.log(`SERVER IS LISTENING http://${hostname}:${PORT}`);
})

module.exports = app